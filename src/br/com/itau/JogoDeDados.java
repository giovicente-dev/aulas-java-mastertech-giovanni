package br.com.itau;

import java.util.Random;

public class JogoDeDados {
    public static void main(String[] args) {
        Random numeroSorteado = new Random();
        int somaNumerosDoDado = 0;

        //Parte 1: sorteando um numero de 1 a 6 e exibindo no console
        System.out.println("***********************************");
        System.out.println("Parte 1");
        int numeroDoDado = numeroSorteado.nextInt(6) + 1;
        System.out.println(numeroDoDado);

        //Parte 2: Sortear 3 numeros entre 1 e 6 e exibir no console a soma destes numeros
        System.out.println("***********************************");
        System.out.println("Parte 2");
        for (int i = 0; i < 3; i++) {
            numeroDoDado = numeroSorteado.nextInt(6) + 1;
            System.out.print(numeroDoDado + ", ");

            somaNumerosDoDado = somaNumerosDoDado + numeroDoDado;
        }
        System.out.println(somaNumerosDoDado);

        //Parte 3: Criar 3 grupos de 3 numeros com as respectivas somas dos numeros de cada grupo
        System.out.println("***********************************");
        System.out.println("Parte 3");
        somaNumerosDoDado = 0;
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                numeroDoDado = numeroSorteado.nextInt(6) + 1;
                System.out.print(numeroDoDado + ", ");

                somaNumerosDoDado = somaNumerosDoDado + numeroDoDado;

                if(j == 2) {
                    System.out.println(somaNumerosDoDado);
                    somaNumerosDoDado = 0;
                }
            }
        }
    }
}
